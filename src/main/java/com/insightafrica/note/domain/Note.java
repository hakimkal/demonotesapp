package com.insightafrica.note.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;



import java.text.SimpleDateFormat;

import java.util.Date;


/**
 * Created by abdulhakim on 9/24/16.
 */
@Entity
@Data
@Table(name = "notes")
public class Note {
    @Column(name="id")
    private @Id @GeneratedValue Long Id;
    private String title;

    @Column(name="note", columnDefinition = "Text")
    private String note;
    // Will be mapped as DATETIME

    @Column(name="created", columnDefinition = "TIMESTAMP")
    private  String created;

    @Column(name="updated", columnDefinition="TIMESTAMP")

    private  String updated;

    private @Version @JsonIgnore Long version;

    private Note(){}
    public Note(String title, String note){

         Date dt = new  Date();

        SimpleDateFormat sdf = new  SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String currentTime = sdf.format(dt);

        this.title = title;
        this.note = note;

        this.created =  currentTime;
        this.updated =  currentTime;

    }
}
