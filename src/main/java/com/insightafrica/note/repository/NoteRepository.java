package com.insightafrica.note.repository;

/**
 * Created by abdulhakim on 9/24/16.
 */
import com.insightafrica.note.domain.Note;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface NoteRepository extends PagingAndSortingRepository<Note , Long> {
    List<Note> findByTitle(String title);
    //Note findByTitle(String title)


}
