package com.insightafrica.note.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by abdulhakim on 9/24/16.
 */

@Controller
public class HomeController {

    @RequestMapping(value="/")
    public String index(){
        return "index";
    }



}
