package com.insightafrica.note.config;

/**
 * Created by abdulhakim on 9/24/16.
 */
import static com.insightafrica.note.config.WebSocketConfiguration.*;

import com.insightafrica.note.domain.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterDelete;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.hateoas.EntityLinks;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component

@RepositoryEventHandler(Note.class)
public class EventHandler {

    private final SimpMessagingTemplate websocket;

    private final EntityLinks entityLinks;

    @Autowired
    public EventHandler(SimpMessagingTemplate websocket, EntityLinks entityLinks) {
        this.websocket = websocket;
        this.entityLinks = entityLinks;
    }

    @HandleAfterCreate
    public void newNote(Note note) {
        this.websocket.convertAndSend(
                MESSAGE_PREFIX + "/newNote", getPath(note));
    }

    @HandleAfterDelete
    public void deleteNote(Note note) {
        this.websocket.convertAndSend(
                MESSAGE_PREFIX + "/deleteNote", getPath(note));
    }

    @HandleAfterSave
    public void updateNote(Note note) {
        this.websocket.convertAndSend(
                MESSAGE_PREFIX + "/updateNote", getPath(note));
    }

    /**
     * Take a {@link Note} and get the URI using Spring Data REST's {@link EntityLinks}.
     *
     * @param note
     */
    private String getPath(Note note) {
        return this.entityLinks.linkForSingleResource(note.getClass(),
                note.getId()).toUri().getPath();
    }

}
