package com.insightafrica.note.config;

import com.insightafrica.note.domain.Note;
import com.insightafrica.note.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by abdulhakim on 9/24/16.
 */
@Component
@RepositoryEventHandler(Note.class)
public class RestAPIEventHandler {

    private final NoteRepository noteRepository;

    @Autowired
    public  RestAPIEventHandler(NoteRepository nrp) {
        this.noteRepository = nrp;
    }

    @HandleBeforeCreate
    public void applyCreatedDate(Note note) {
        Date dt = new  Date();

        SimpleDateFormat sdf = new  SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String currentTime = sdf.format(dt);

        note.setCreated(currentTime);
        note.setUpdated(currentTime);
    }
}