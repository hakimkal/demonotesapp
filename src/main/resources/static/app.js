/**
 * Created by Abdulhakim on 9/20/2016.
 */
const React= require('react');
const client = require('./client');
const follow = require('./follow');
const ReactDom = require('react-dom');
const when = require('when');
import CreateDialog from './components/CreateDialog';
import NoteList from './components/NoteList';
const root =  window.config.baseUrl + 'api';
const stompClient = require('./websocket-listener');


class App extends React.Component{


    constructor(props) {
        super(props);
        this.state = {notes: [], attributes: [], pageSize: 3, links: {}, lastNotes: []};


        this.updatePageSize = this.updatePageSize.bind(this);
        this.onCreate = this.onCreate.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onNavigate = this.onNavigate.bind(this);
        this.refreshCurrentPage = this.refreshCurrentPage.bind(this);
        this.refreshAndGoToLastPage = this.refreshAndGoToLastPage.bind(this);
    }
    componentDidMount(){


            this.loadFromServer(this.state.pageSize);

        stompClient.register([
            {route: '/topic/newNote', callback: this.refreshAndGoToLastPage},
            {route: '/topic/updateNote', callback: this.refreshCurrentPage},
            {route: '/topic/deleteNote', callback: this.refreshCurrentPage}
        ]);
    }


    // tag::follow-2[]
    loadFromServer(pageSize) {
        follow(client, root, [
            {rel: 'notes', params: {size: pageSize, sort:"title,desc" }}]
        ).then(noteCollection => {
            return client({
                method: 'GET',
                path: noteCollection.entity._links.profile.href,
                headers: {'Accept': 'application/schema+json'}
            }).then(schema => {
                // tag::json-schema-filter[]
               // console.log("The Schema: " + JSON.stringify(schema));
                /**
                 * Filter unneeded JSON Schema properties, like uri references and
                 * subtypes ($ref).
                 */
                Object.keys(schema.entity.properties).forEach(function (property) {
                if (schema.entity.properties[property].hasOwnProperty('format') &&
                    schema.entity.properties[property].format === 'uri') {
                    delete schema.entity.properties[property];
                }
                    if (schema.entity.properties[property].title === "Updated"){
                        delete schema.entity.properties[property];
                    }
/*
                if (schema.entity.properties[property].hasOwnProperty('$ref')) {
                    delete schema.entity.properties[property];
                }*/

            });

        this.schema = schema.entity;
        this.links = noteCollection.entity._links;
        return noteCollection;
        // end::json-schema-filter[]
    });
    }).then(noteCollection => {
            this.page = noteCollection.entity.page;

        this.setState({

            notes:noteCollection.entity._embedded.notes,

        });
        return noteCollection.entity._embedded.notes.map(note =>
            client({
                method: 'GET',
                path: note._links.self.href
            })
    );
    }).then(notePromises => {
            return when.all(notePromises);
    }).then(notes => {
         //   console.log(notes);
        this.setState({
            page: this.page,
            lastNotes: notes,
            attributes: Object.keys(this.schema.properties),
            pageSize: pageSize,
            links: this.links
        });
    });
    }



    // end::follow-2[]

    // tag::create[]
    onCreate(newNote) {
        follow(client, root, ['notes']).then(employeeCollection => {
            return client({
                method: 'POST',
                path: employeeCollection.entity._links.self.href,
                entity: newNote,
                headers: {'Content-Type': 'application/json'}
            })
        }).then(response => {
            return follow(client, root, [
                {rel: 'notes', params: {'size': this.state.pageSize}}]);
    }).then(response => {
            this.onNavigate(response.entity._links.last.href);
    });
    }
    // end::create[]

    // tag::delete[]
            onDelete(note) {
                client({method: 'DELETE', path: note._links.self.href}
                ).then(response => {/* let the websocket handle updating the UI */},
                    response => {
                    if (response.status.code === 403) {
                        alert('ACCESS DENIED: You are not authorized to delete ' +
                            note._links.self.href);
                    }
                });
            }
    // end::delete[]



    // tag::navigate[]
    onNavigate(navUri) {
        client({
            method: 'GET',
            path: navUri
        }).then(noteCollection => {
            this.links = noteCollection.entity._links;
        this.setState({
            notes: noteCollection.entity._embedded.notes,

        });
        return noteCollection.entity._embedded.notes.map(note =>
            client({
                method: 'GET',
                path: note._links.self.href
            })
    );
    }).then(notePromises => {
            return when.all(notePromises);
    }).then(notes => {
            this.setState({
            LastNotes: notes,
            attributes: Object.keys(this.schema.properties),
            pageSize: this.state.pageSize,
            links: this.links
        });
    });
    }
    // end::navigate[]

    // tag::update-page-size[]
    updatePageSize(pageSize) {
        if (pageSize !== this.state.pageSize) {
            this.loadFromServer(pageSize);
        }
    }
    // end::update-page-size[]
//tag::update
            onUpdate(note, updatedNote) {
                client({
                    method: 'PUT',
                    path: note.entity._links.self.href,
                    entity: updatedNote,
                    headers: {
                        'Content-Type': 'application/json',
                        'If-Match': note.headers.Etag
                    }
                }).then(response => {
                   // console.log(response);
                    /* Let the websocket handler update the state */
                }, response => {
                    if (response.status.code === 403) {
                        alert('ACCESS DENIED: You are not authorized to update ' +
                            note.entity._links.self.href);
                    }
                    if (response.status.code === 412) {
                        alert('DENIED: Unable to update ' + note.entity._links.self.href +
                            '. Your copy is outdated.');
                    }
                });
            }
    //tag::end-update


    refreshAndGoToLastPage(message) {
        follow(client, root, [{
            rel: 'notes',
            params: {size: this.state.pageSize}
        }]).then(response => {
            if (response.entity._links.last !== undefined) {
            this.onNavigate(response.entity._links.last.href);
        } else {
            this.onNavigate(response.entity._links.self.href);
        }
    })
    }

    refreshCurrentPage(message) {
        follow(client, root, [{
            rel: 'notes',
            params: {
                size: this.state.pageSize,
                page: this.state.page.number
            }
        }]).then(employeeCollection => {
            this.links = employeeCollection.entity._links;
        this.page = employeeCollection.entity.page;
        this.setState({
            page: this.page,
            notes: employeeCollection.entity._embedded.notes,

        });
        return employeeCollection.entity._embedded.notes.map(note => {
                return client({
                    method: 'GET',
                    path: note._links.self.href
                })
            });
    }).then(notePromises => {
            return when.all(notePromises);
    }).then(notes => {
            this.setState({
            page: this.page,
            LastNotes: notes,
            attributes: Object.keys(this.schema.properties),
            pageSize: this.state.pageSize,
            links: this.links
        });
    });
    }
        render() {

            return (
                <div>
                <CreateDialog attributes={this.state.attributes} onCreate={this.onCreate}/>
        <NoteList notes={this.state.notes} lastNotes={this.state.lastNotes} attributes={this.state.attributes}  links={this.state.links}
            pageSize={this.state.pageSize}
            onNavigate={this.onNavigate}
            onDelete={this.onDelete}
            updatePageSize={this.updatePageSize}
            onUpdate={this.onUpdate}
        />
        </div>
        )
        }
    }




ReactDom.render(
    <App />,
    document.getElementById('react')
)


