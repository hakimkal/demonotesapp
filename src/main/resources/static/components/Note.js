import React from 'react';
import UpdateDialog from './UpdateDialog';

class Note extends React.Component{
    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete() {
        this.props.onDelete(this.props.note);
    }

    render() {

        return (

            <tr id={"note-" + this.props.index} >
            <td >{this.props.note.title}</td>
        <td id={"entityID-" + this.props.index} style={{display:'none'}}>{this.props.note.id}</td>
        <td>{this.props.note.note}</td>
        <td>{this.props.note.created}</td>


        <td>
        <UpdateDialog index={this.props.index} lastNote={this.props.lastNote} note={this.props.note}
        attributes={this.props.attributes}
        onUpdate={this.props.onUpdate}/>
    </td>
        <td>
        <input type="submit" id={"deleteButton-"+this.props.index} className="btn waves-effect waves-light"  onClick={this.handleDelete} value="Delete"  />
        </td>
        </tr>
    )
    }
}

export default Note