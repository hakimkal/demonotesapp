/**
 * Created by abdulhakim on 9/23/16.
 */
import React from 'react';
import ReactDom from 'react-dom';
import Note from './Note';

class NoteList extends React.Component{

    constructor(props) {
        super(props);
        this.handleNavFirst = this.handleNavFirst.bind(this);
        this.handleNavPrev = this.handleNavPrev.bind(this);
        this.handleNavNext = this.handleNavNext.bind(this);
        this.handleNavLast = this.handleNavLast.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }

    // tag::handle-page-size-updates[]
    handleInput(e) {
        e.preventDefault();
        var pageSize = ReactDom.findDOMNode(this.refs.pageSize).value;
        if (/^[0-9]+$/.test(pageSize)) {
            this.props.updatePageSize(pageSize);
        } else {
            ReactDom.findDOMNode(this.refs.pageSize).value =
                pageSize.substring(0, pageSize.length - 1);
        }
    }
    // end::handle-page-size-updates[]

    // tag::handle-nav[]
    handleNavFirst(e){
        e.preventDefault();
        this.props.onNavigate(this.props.links.first.href);
    }

    handleNavPrev(e) {
        e.preventDefault();
        this.props.onNavigate(this.props.links.prev.href);
    }

    handleNavNext(e) {
        e.preventDefault();
        this.props.onNavigate(this.props.links.next.href);
    }

    handleNavLast(e) {
        e.preventDefault();
        this.props.onNavigate(this.props.links.last.href);
    }
    // end::handle-nav[]

    // tag::employee-list-render[]

    render() {


        var notes = this.props.notes.map((note, index) =>{
return(
            <Note key={note._links.self.href} index={index}  lastNote={this.props.lastNotes[index]}  note={note}  attributes={this.props.attributes} onUpdate={this.props.onUpdate} onDelete={this.props.onDelete}/>);

        }
    );



        var navLinks = [];
        if ("first" in this.props.links) {
            navLinks.push(<button key="first" onClick={this.handleNavFirst}>&lt;&lt;</button>);
        }
        if ("prev" in this.props.links) {
            navLinks.push(<button key="prev" onClick={this.handleNavPrev}>&lt;</button>);
        }
        if ("next" in this.props.links) {
            navLinks.push(<button key="next" onClick={this.handleNavNext}>&gt;</button>);
        }
        if ("last" in this.props.links) {
            navLinks.push(<button key="last" onClick={this.handleNavLast}>&gt;&gt;</button>);
        }
       var thStyle = {display:'none' };
        return (
            <div>
            <input ref="pageSize" defaultValue={this.props.pageSize} onInput={this.handleInput}/>
    <table className="striped">
            <thead>
        <tr>
        <th>Title</th>
            <th style={thStyle}>The ID</th>
        <th>Note</th>
        <th>Created</th>

        <th></th>
        </tr>
            </thead>
            <tbody>
        {notes}</tbody>
        </table>
        <div>
        {navLinks}
        </div>
        </div>
    )
    }
}

export default NoteList;