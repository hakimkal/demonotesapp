/**
 * Created by abdulhakim on 9/23/16.
 */
import React from 'react';
import ReactDom from 'react-dom';
class CreateDialog extends React.Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        var newNote = {};


        this.props.attributes.forEach(attribute => {
            if(attribute != 'id' && attribute !='created')
        {
            newNote[attribute] = ReactDom.findDOMNode(this.refs[attribute]).value.trim();
        }
    });
        this.props.onCreate(newNote);

        // clear out the dialog's inputs
        this.props.attributes.forEach(attribute => {
            if(attribute != 'id' && attribute !='created')
        {
            ReactDom.findDOMNode(this.refs[attribute]).value = '';
        }
    });

        // Navigate away from the dialog to hide it.
        window.location = "#";
    }

    render() {
        var inputs = this.props.attributes.sort().map((attribute) =>{
                if(attribute != 'id' && attribute != 'created'){
            return(<p key={attribute}>
            <input type="text" id={attribute} placeholder={attribute} ref={attribute} className="field" />
            </p>);}}
    );

        return (
            <div>
            <a id="createNoteLink" className="btn waves-effect waves-light" href="#createNote">Create Note</a>

            <div id="createNote" className="modalDialog">
            <div>
            <a href="#" title="Close" className="close">X</a>

            <h2>Create a note</h2>

        <form onSubmit={this.handleSubmit}>
        {inputs}
        <input type="submit" id="submitNewNote" onClick={this.handleSubmit} value="Create" />
        </form>
        </div>
        </div>
        </div>
    )
    }

}
export default CreateDialog;