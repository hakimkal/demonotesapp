/**
 * Created by abdulhakim on 9/23/16.
 */
import React from 'react';
import ReactDom from 'react-dom';

class UpdateDialog extends React.Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        var updatedNote = {};
        this.props.attributes.forEach(attribute => {
            updatedNote[attribute] = ReactDom.findDOMNode(this.refs[attribute]).value.trim();
    });
        //console.log("The note object: \n" + this.props.note);
        this.props.onUpdate(this.props.lastNote, updatedNote);
        window.location = "#";
    }

    render() {
        var inputs = this.props.attributes.map(attribute =>
            <p key={this.props.note[attribute]}>
    <input type="text" id={attribute + "-field-" + this.props.index} placeholder={attribute}
        defaultValue={this.props.note[attribute]}
        ref={attribute} className="field" />
            </p>
    );

        var dialogId = "updateNote-" + this.props.note._links.self.href;

        return (
            <div key={this.props.note._links.self.href}>
    <a id={"updateNoteLink" + this.props.index} href={"#" + dialogId} className="btn waves-effect waves-light" >Update Note</a>
            <div id={dialogId} className="modalDialog">
            <div>
            <a href="#" title="Close" className="close">X</a>

            <h2>Update an employee</h2>

        <form onSubmit={this.handleSubmit}>
        {inputs}
        <input type="submit" id={"submitUpdatedNote-" + this.props.index} onClick={this.handleSubmit} value="Update" />
        </form>
        </div>
        </div>
        </div>
    )
    }

};

export default UpdateDialog;