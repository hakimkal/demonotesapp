Feature: Delete Note
  Scenario: Delete note
    Given User is on Home Page
    And The table has one or more notes
    When User selects the first note on the table
    And  User  Click first Delete button
    Then The note should be deleted