Feature: Update Note
  Scenario: Update Note
    Given User is on Home Page
    When   When User Click on first Update button
    And   User gets note and title
    And User changes note and title
    Then  Message displayed note updated successfully
