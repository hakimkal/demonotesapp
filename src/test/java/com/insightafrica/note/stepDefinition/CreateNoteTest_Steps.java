package com.insightafrica.note.stepDefinition;

import com.insightafrica.note.domain.Note;
import com.insightafrica.note.repository.NoteRepository;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.junit.ScenarioOutlineRunner;
import io.github.bonigarcia.wdm.MarionetteDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.remote.HttpSessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Value;
/**
 * Created by abdulhakim on 9/25/16.
 */



public class CreateNoteTest_Steps  extends AbstractCucumberTest{

    @Autowired
    private NoteRepository noteRepository;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

     public static WebDriver driver = null ;
    public long recordEntityID ;
     public Note noteRecord ;
    public String recordEntityTitle ;
    @Value("${local.server.port}")
    private int port = 9000 ;

    /**/




    @Given("^User is on Home Page$")
    public void user_is_on_Home_Page() throws Throwable {
        MarionetteDriverManager.getInstance().setup();
        System.setProperty("webdriver.gecko.driver", "/home/abdulhakim/geckodriver");
        //driver = new FirefoxDriver();

        driver = new MarionetteDriver();

        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.get("http://localhost:" + port);
        //driver.navigate().to("http://lepsms.org");


    }
        /* Test Create Note Feature */
    @When("^User Click Create link$")
    public void user_Click_Create_link() throws Throwable {

        driver.findElement(By.cssSelector("a[href*='#createNote']")).click();


    }

    @When("^User enters note and title$")
    public void user_enters_note_and_title() throws Throwable {

        Date dt = new  Date();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String currentTime = sdf.format(dt);
        //driver.findElement(By.id("created")).sendKeys(currentTime);

        driver.findElement(By.id("title")).sendKeys("testuser_1");

        driver.findElement(By.id("note")).sendKeys("Test@123");


       driver.findElement(By.id("submitNewNote")).click();
       //driver.findElement(By.id("submitNewNote")).sendKeys(Keys.ENTER);
        System.out.println("Successfully submitted");
    }

    @Then("^Message displayed Created Note Successfully$")
    public void message_displayed_note_created() throws Throwable {
        List<Note> theNotes = this.noteRepository.findByTitle("testuser_1");
        assertEquals("testuser_1",theNotes.get(0).getTitle());
        System.out.println(theNotes.get(0).getTitle());

        //Thread.sleep(20);
         driver.quit();

    }
    /* Test View Notes  Note Feature */
    @Then("^Table body should have at least (\\d+)  record items shown$")
    public void table_body_should_have_at_least_record_items_shown(int arg1) throws Throwable {

        WebElement mainBody = driver.findElement(By.cssSelector("tbody"));
        List<WebElement> tableRows = mainBody.findElements(By.tagName("tr"));
        assertNotNull(tableRows);

        driver.quit();

    }

    /* Test Delete Note Feature */
    @Given("^The table has one or more notes$")
    public void the_table_has_one_or_more_notes() throws Throwable {
        WebElement mainBody = driver.findElement(By.cssSelector("tbody"));
        List<WebElement> tableRows = mainBody.findElements(By.tagName("tr"));
        assertNotNull(tableRows);

    }
    @When("^User selects the first note on the table$")
    public void user_selects_the_first_note_on_the_table() throws Throwable {
        WebElement hiddenTd = driver.findElement(By.id("entityID-0"));


        String script = "return document.getElementById('entityID-0').innerHTML";
        String hiddentTdVal  = (String) ((JavascriptExecutor) driver).executeScript(script, hiddenTd);
        //Set the recordEntityID value
        this.recordEntityID = Long.parseLong(hiddentTdVal);
        System.out.println("The ID is: " + this.recordEntityID);

    }

    @When("^User  Click first Delete button$")
    public void user_Click_first_Delete_button() throws Throwable {


        //System.out.println("Row ID " + this.recordEntityID);
        driver.findElement(By.id("deleteButton-0")).click();


    }


    @Then("^The note should be deleted$")
    public void the_note_should_be_deleted() throws Throwable {

   //Note deletedNote = noteRepository.findOne(this.recordEntityID);
        boolean deletedNote = noteRepository.exists(this.recordEntityID);
       //System.out.println("The deleted note ID:" + deletedNote);
        assertFalse(deletedNote);
        driver.quit();
    }

    /* Test Update  Note Feature */

    @When("^When User Click on first Update button$")
    public void  when_User_Click_on_first_Update_button() throws Throwable {


        WebElement hiddenTd = driver.findElement(By.id("entityID-0"));


        String script = "return document.getElementById('entityID-0').innerHTML";
        String hiddentTdVal  = (String) ((JavascriptExecutor) driver).executeScript(script, hiddenTd);
        //Set the recordEntityID value
        this.recordEntityID = Long.parseLong(hiddentTdVal);


        // Get and Set the recordEntityTitle value
        WebElement theTr = driver.findElement(By.id("note-0"));
         List<WebElement> theTds = theTr.findElements(By.tagName("td"));
        this.recordEntityTitle = theTds.get(0).getText();
        System.out.println("\n The record Title: " + this.recordEntityTitle);
       driver.findElement(By.id("updateNoteLink0")).click();




    }

    @When("^User gets note and title$")
    public void user_gets_note_and_title() throws Throwable {
        //title:testuser_1 note: Test@123

        driver.findElement(By.id("title")).getText().contentEquals(this.recordEntityTitle);

        // There is no need for the note
        //driver.findElement(By.id("note")).getText().contentEquals("Test@123");
    }

    @When("^User changes note and title$")
    public void user_changes_note_and_title() throws Throwable {

       // driver.findElement(By.id("note-0")).findElement(By.tagName("form"));

        driver.findElement(By.id("note-field-0")).clear();

        driver.findElement(By.id("note-field-0")).sendKeys("New_Test@123");
        driver.findElement(By.id("title-field-0")).clear();
        driver.findElement(By.id("note-0")).findElement(By.id("title-field-0")).sendKeys("new_testuser_1");


        driver.findElement(By.id("note-0")).findElement(By.id("submitUpdatedNote-0")).click();


    }

    @Then("^Message displayed note updated successfully$")
    public void message_displayed_note_updated_successfully() throws Throwable {
        Note theNotes = this.noteRepository.findOne(this.recordEntityID);
        assertEquals("New_Test@123",theNotes.getNote());
        System.out.println(theNotes.getTitle());
        driver.quit();
    }



}
