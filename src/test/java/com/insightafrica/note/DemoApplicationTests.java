package com.insightafrica.note;

import com.insightafrica.note.domain.Note;
import com.insightafrica.note.repository.NoteRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
//@RunWith(SpringRunner.class)
@SpringBootTest


public class DemoApplicationTests {
    private NoteRepository noteRepository;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    public void setNoteRepository(NoteRepository productService) {
        this.noteRepository = productService;
    }

    @Test
    public void testCreateNote() {
        logger.info("Testing create note unit");
        Note note = this.noteRepository.save(new Note("Mickey Mouse", "Mickey stories"));
        assertEquals(note.getTitle(), "Mickey Mouse");

    }

    @Test
    public void testUpdateNote() {
        logger.info("Testing update note unit");
        //Create new notes
        Note note = this.noteRepository.save(new Note("Mickey Mouse 1", "Mickey stories one"));

        //get  a note
        note = this.noteRepository.findOne(note.getId());

        //update the note
        note.setTitle("Change title for old mickey");
        this.noteRepository.save(note);
        //assertupdated
        assertEquals(note.getTitle(), "Change title for old mickey");
    }

    @Test
    public void testDeleteNote() {
        logger.info("Testing delete note unit");
        //Create new notes
        Note note = this.noteRepository.save(new Note("Mickey Mouse 11", "Mickey stories eleven"));

        //delete the note
        this.noteRepository.delete(note.getId());

        //get the note
        Note delNote = this.noteRepository.findOne(note.getId());
        assertNull(delNote);




    }

    @Test
    public void testListNotes() {
        logger.info("Testing notes listing unit ");
        //Delete the table records
        this.noteRepository.deleteAll();
        //Create new notes
        Note note = this.noteRepository.save(new Note("Mickey Mouse 1", "Mickey stories one"));
        Note note2 = this.noteRepository.save(new Note("Mickey Mouse 2", "Mickey stories two"));
//Retrieve the two notes

        assertEquals(2, this.noteRepository.count());
    }


}
