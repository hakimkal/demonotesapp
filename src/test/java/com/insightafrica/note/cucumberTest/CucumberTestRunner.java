package com.insightafrica.note.cucumberTest;
//package com.insightafrica.note.stepDefinition;
//package com.insightafrica.note;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


/**
 * Created by abdulhakim on 9/25/16.
 */


@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/com/insightafrica/note/Feature"
        , glue = "com.insightafrica.note.stepDefinition", format = "pretty", dryRun = true)
public class CucumberTestRunner {

}
